<?php
/**
 * Product extension
 * 
 * 
 *
 * @category   Neo
 * @package    Product 
 * @author     Shailendra
 */
//Mage::setIsDeveloperMode(true);

//Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

class Neo_Product_ManageController extends Mage_Core_Controller_Front_Action
{

    /**
    * listAction
    *
    * @author    Shailendra Gupta
    * @access    public
    * @params    null
    * @return    void
    **/
    
    //function to list all the products of customer
    public function listAction()
    {
        if(!Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->_redirect('customer/account/login');
            return;
        }         
        $this->loadLayout();     
        $this->renderLayout();
    }
	
    /**
    * addAction
    *
    * @author    Aman Juneja
    * @access    public
    * @params    null
    * @return    void
    **/
    
    //function to add products of customer
	
	public function addAction()
    {
        if(!Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->_redirect('customer/account/login');
            return;
        }         
        $this->loadLayout();     
        $this->renderLayout();
    }
	

    /**
    * saveproductAction
    *
    * @author    Shailendra Gupta
    * @access    public
    * @params    null
    * @return    void
    **/
    
    //function to save a product
    public function saveproductAction()
    {
        if(Mage::getSingleton('customer/session')->isLoggedIn()) {
        	$seller_email = Mage::getSingleton('customer/session')->getCustomer()->getEmail();
			$seller_name = Mage::getSingleton('customer/session')->getCustomer()->getName();
			$seller_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
            $productData = $this->getRequest()->getPost('product');
			$files = $_FILES['photo']['name'];
			$appEmulation = Mage::getSingleton('core/app_emulation');//Start environment emulation of the specified store
            $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation(Mage_Core_Model_App::ADMIN_STORE_ID);
            $product = new Mage_Catalog_Model_Product();
            // Build the product
            $product->setSku($productData['sku']);
            $product->setAttributeSetId('4');
            $product->setTypeId('simple');
            $product->setName($productData['name']);
			$product->setCategoryIds(array(3, $productData['parent_category'],$productData['sub_category'])); # some cat id's, my is 7
			$product->setWebsiteIDs(array(1)); # Website id, my is 1 (default frontend)
            $product->setDescription($productData['description']);
            $product->setShortDescription($productData['short_description']);
            $product->setPrice($productData['price']); # Set some price
            //Default Magento attribute
            $product->setWeight($productData['weight']);
            $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
            $product->setStatus($productData['status']);
            $_product = Mage::getModel('catalog/product');
            $attr = $_product->getResource()->getAttribute("manufacturer");
            if ($attr->usesSource()) {
                $manufacturer_id = $attr->getSource()->getOptionId(Mage::getSingleton('customer/session')->getCustomer()->getEmail());
            } 
			if($productData['price']=='0'){
				$product->setFreeStuff('1');	
			}
            $product->setManufacturer($manufacturer_id);
            //$product->setCondition($productData['condition']);
            //$product->setPayment($productData['payment']);
			//$product->setState($productData['state']);
			//$product->setCustomerEmail($seller_email);
			//$product->setCustomerId($seller_id);
			//$product->setCustomerName($seller_name);
			//$product->setCustomerMobile($productData['mobile']);
			//$product->setSuburb($productData['suburb']);
            $product->setTaxClassId(0); # My default tax class
            $product->setStockData(array(
                'is_in_stock' => 1,
                'qty' => 99999
            ));
			$product->setMediaGallery(array('images'=>array (), 'values'=>array ()));                   
			//$this->setImagePath();		
            if(isset($_FILES['photo']))           
              if(is_array($_FILES['photo']['name']))
             {
	  
				try
			   {
				 for($i=0;$i<count($_FILES['photo']['name']);$i++)
				 {
					if($_FILES['photo']['error'][$i]!=4)
					    {                             	               	
							$imagePath = $this->imagePath.trim($_FILES['photo']['name'][$i]);	   
							move_uploaded_file($_FILES['photo']['tmp_name'][$i], $imagePath);                  
							$imagePath = $this->imagePath.trim($_FILES['photo']['name'][$i]);
							if($i==0){
								$product->addImageToMediaGallery($imagePath, array('image','small_image','thumbnail'),false,false);
								}
							else{
								$product->addImageToMediaGallery($imagePath, array(),false,false);     
								}
						 }
				  }
				  }catch(Exception $e){
						echo $e->getMessage(); 	  
				  }
		     }
						
			
			
			$product->setCreatedAt(strtotime('now'));
            try {
                $product->save();
                Mage::getSingleton('core/session')->addSuccess('Product Created Successfully'); 
                $this->_redirect('product/manage/list');
            }
            catch (Exception $ex) {
                Mage::getSingleton('core/session')->addError($ex->getMessage()); 
                $this->_redirect('product/manage/list');
                //Handle the error
            } 

            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
        }else{
            Mage::getSingleton('core/session')->addError('Please login to continue'); 
            $this->_redirect('customer/account/login');
            return;            
        }
    }    

    /**
    * getChildCategoriesAction
    *
    * @author    Shailendra Gupta
    * @access    public
    * @params    null
    * @return    void
    **/

    //function to fetch Child Categories
    public function getChildCategoriesAction()
    {
        if(!Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->_redirect('customer/account/login');
            return;
        } 
        $catId = $this->getRequest()->getPost('categoryId');
        $curCategory = Mage::getModel('catalog/category')->load($catId);
        $catChildren = $curCategory->getChildren();
        $arrCatChildren = explode(',',$catChildren);
        $arrCatList = array();
        foreach($arrCatChildren as $sub_subCatid)
        {
            $_sub_category = Mage::getModel('catalog/category')->load($sub_subCatid);
            if($_sub_category->getIsActive()) {
                $arrCatList[] = array('catid' => $_sub_category->getId(), 'catname' => $_sub_category->getName());
            }
        }        
        echo json_encode($arrCatList);     
    }
    
    public function demoAction()
    {
		$this->loadLayout();
		$this->renderLayout();
	}

    /**
    * testAction
    *
    * @author    Shailendra Gupta
    * @access    public
    * @params    null
    * @return    void
    **/

    //function to create a product
    public function testAction()
    {
    	 
        echo "here";
        //exit;
        //$product = Mage::getModel('catalog/product');
        $product = new Mage_Catalog_Model_Product();
        // Build the product
        $product->setSku('123');
        $product->setEntityId('5');
        $product->setAttributeSetId('4');
        $product->setTypeId('simple');
        $product->setName('Some cool product name');
        $product->setCategoryIds(array(3)); # some cat id's, my is 7
        $product->setWebsiteIDs(array(1)); # Website id, my is 1 (default frontend)
        $product->setDescription('Full description here');
        $product->setShortDescription('Short description here');
        $product->setPrice(39.99); # Set some price
        //Default Magento attribute
        $product->setWeight(4.0000);
        $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        $product->setStatus(1);
        $product->setTaxClassId(0); # My default tax class
        $product->setStockData(array(
            'is_in_stock' => 1,
            'qty' => 99999
        ));
        $product->setCreatedAt(strtotime('now'));
        try {
            $product->save();
        }
        catch (Exception $ex) {
            echo "<pre>"; print_r($ex->getMessage()); exit('exit');
            //Handle the error
        }        
    }

	  /**
    * setSoldAction
    *
    * @author    Aman Juneja
    * @access    public
    * @params    null
    * @return    void
    **/

    public function setSoldAction(){
    	
		$product_id = $this->getRequest()->getPost('prod_id');
		try{
			$setsold = Mage::getModel('catalog/product')->load($product_id);
			$setsold = $setsold->setStockData(array(
				'is_in_stock' => 0,
				'qty' => 0
			));
            $setsold->setSold(1);
			$setsold->save();
			echo "Product status set Sold";
		}
		catch(Exception $e){
			echo "Setting Sold Status Failed";
		}	
			
    }
	
	  /**
    * setDeleteAction
    *
    * @author    Aman Juneja
    * @access    public
    * @params    null
    * @return    void
    **/
    
	public function setDeleteAction(){
    	
		$product_id = $this->getRequest()->getPost('prod_id');
		//$setsold = Mage::getModel('catalog/product')->load($product_id);
		try{
				Mage::register('isSecureArea', true);
    			Mage::getModel("catalog/product")->load($product_id)->delete();
				Mage::unregister('isSecureArea');
			    echo "Product Deleted Successfully";	
			}catch(Exception $e){
    			echo "Delete failed";
			}
	}

}	
