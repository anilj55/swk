<?php
class Neo_Product_Block_Product extends Mage_Core_Block_Template
{

	public function _prepareLayout()
    {
		return parent::_prepareLayout();
		
    }

    /**
    * getAttribute
    *
    * @author    Shailendra Gupta
    * @access    public
    * @params    null
    * @return    void
    **/

    //function to attribute data by attribute code

    public function getAttribute($attrCode)     
    { 
        $entityType = 'catalog_product';
        $arrAttr = array();
        $attr = Mage::getModel('eav/config')->getAttribute($entityType,$attrCode);
        return $attrData = $attr->getSource()->getAllOptions();
    }

    /**
    * getCategory
    *
    * @author    Shailendra Gupta
    * @access    public
    * @params    null
    * @return    void
    **/

    //function to category data by category code

    public function getCategory($catId)     
    { 
        $curCategory = Mage::getModel('catalog/category')->load($catId);
        $catChildren = $curCategory->getChildren();
        $arrCatChildren = explode(',',$catChildren);
        $arrCatList = array();
        foreach($arrCatChildren as $sub_subCatid)
        {
            $_sub_category = Mage::getModel('catalog/category')->load($sub_subCatid);
            if($_sub_category->getIsActive()) {
                $arrCatList[] = array('id' => $_sub_category->getId(), 'name' => $_sub_category->getName());
            }
        }        
        return $arrCatList;
    }
	
	 /**
    * getProductList
    *
    * @author    Aman Juneja
    * @access    public
    * @params    null
    * @return    void
    **/

    //function to get lsit of products for logged in seller
	
	public function getProductList(){
		if(Mage::getSingleton('customer/session')->isLoggedIn()){
			$email = Mage::getSingleton('customer/session')->getCustomer()->getEmail();	
			$product_coll = Mage::getModel('catalog/product')->getCollection()
							->addAttributeToSelect('*')
							->addAttributeToFilter('manufacturer',array('eq'=>$email))
							->addAttributeToFilter('visibility',4)
							->addAttributeToFilter('status',1)
							->load();
			return $product_coll;		
		
		}
	}
	

}