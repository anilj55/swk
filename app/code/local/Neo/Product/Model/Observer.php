<?php
class Neo_Product_Model_Observer
{
    public function registering_manufacturer($observer){
        $customerData = $observer->getEvent()->getCustomer()->getData();
        //Get the eav attribute model
        $attr_model = Mage::getModel('catalog/resource_eav_attribute');

        //Load the particular attribute by id
        //Here 81 is the id of 'manufacturer' attribute
        $attr_model->load(81);

        //Create an array to store the attribute data
        $data = array();

        //Create options array
        $values = array(array(0 => $customerData['email']));

        //Add the option values to the data
        $data['option']['value'] = $values;

        //Add data to our attribute model
        $attr_model->addData($data);
        $attr_model->save();
        
    }
}